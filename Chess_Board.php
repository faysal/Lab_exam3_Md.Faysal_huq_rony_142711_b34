<!doctype html>
<html lang="en">
<head>
    <title>Chess_Board</title>
</head>
<style>
    .main{
        width: 800px;
        margin: 0 auto;
        padding: 0;
        border: 1px solid black;
    }
    .black{
        height:100px;
        width:100px;
        background-color: black;
        display: inline-block;
        margin: 0;
        padding: 0;
        vertical-align: bottom;
    }
    .red{
        height:100px;
        width:100px;
        background-color: white;
        display: inline-block;
        margin: 0;
        padding: 0;
        vertical-align: bottom;
    }
</style>
<body bgcolor="#2c3E54">
<div class="main">
    <?php
    for($i = 1; $i <= 8 ; $i++){
        for($a = 1; $a <= 8; $a++){
            if($i%2 == 0) {
                if($a%2 == 0){
                    echo "<div class='black' > </div>";
                }else{
                    echo "<div class='red' > </div>";
                }
            }else{
                if($a%2 == 0){
                    echo "<div class='red' > </div>";
                }else{
                    echo "<div class='black' > </div>";
                }
            }
        }
    }
    ?>
</div>
</body>
</html>
